#!/bin/bash
# By Stephen Vanderwarker
# http://new.scuba323.com/me/

# Set date/time format
now=$(date +"%T")

# Ask user what site to check
echo "Please enter site (no http(s)://)"
read site

# Ping said site and drop output
if ping -c 1 scuba323.com > /dev/null
# If up then yay!
then
notify-send "YAY" "$site  is all good - $now" -i notification-message-im && paplay ./notification.ogg
echo "Site is all good"
# If down warn user.
else
notify-send "WARNING" "$site is down - $now" -i notification-message-im && paplay ./alert.ogg
echo "Oh no! :( Site's down (or your internet connection is)"
# See if anyone else can reach it... Maybe do an API later on (if there is one)
echo "Please try later, or check http://isup.me/$site"
# WE DONE
fi
